package com.mobtaker.mykotlinapplication.repository

import com.mobtaker.mykotlinapplication.data.dao.CharacterDao
import com.mobtaker.mykotlinapplication.domain.RemoteRepository
import com.mobtaker.mykotlinapplication.utills.performGetOperation
import javax.inject.Inject

class Repository @Inject constructor(private val remoteRepository: RemoteRepository, private val localRepository: CharacterDao)
{

    fun getCharacter(id: Int) = performGetOperation(
        databaseQuery = { localRepository.getCharacter(id) },
        networkCall = { remoteRepository.getCharacter(id) },
        saveCallResult = { localRepository.insert(it) })

    fun getCharacters() = performGetOperation(
        databaseQuery = { localRepository.getAllCharacters() },
        networkCall = { remoteRepository.getCharacters() },
        saveCallResult = { localRepository.insertAll(it.results) }
    )
}