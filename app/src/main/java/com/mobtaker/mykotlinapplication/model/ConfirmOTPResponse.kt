package com.mobtaker.mykotlinapplication.model

import com.google.gson.annotations.SerializedName
data class ConfirmOTPResponse(
    @SerializedName("token") val token: String,
    @SerializedName("is_new") val is_new: Boolean,
    @SerializedName("user") val user:User
)