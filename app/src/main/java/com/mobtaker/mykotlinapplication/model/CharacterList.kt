package com.mobtaker.mykotlinapplication.model



data class CharacterList(
    val info: Info,
    val results: List<Character>
    )