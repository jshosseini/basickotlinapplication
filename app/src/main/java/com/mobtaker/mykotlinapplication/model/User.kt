package com.mobtaker.mykotlinapplication.model

import com.google.gson.annotations.SerializedName

class User(
    @SerializedName("id") val id: Int,
    @SerializedName("mobile") val mobile: String,
    @SerializedName("display_name") val displayName: String,
    @SerializedName("gender") val gender: String,
    @SerializedName("avatar") val avatar: String?,
    )