package com.mobtaker.mykotlinapplication.domain

import com.mobtaker.mykotlinapplication.model.CharacterList
import com.mobtaker.mykotlinapplication.model.Character
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface RequestApi {
    @GET("character")
    suspend fun getAllCharacters(): Response<CharacterList>

    @GET("character/{id}")
    suspend fun getCharacter(@Path("id") id: Int): Response<Character>
}