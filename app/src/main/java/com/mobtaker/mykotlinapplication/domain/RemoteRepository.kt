package com.mobtaker.mykotlinapplication.domain


import javax.inject.Inject

class RemoteRepository @Inject constructor(
    private val requestApi: RequestApi
): BaseDataSource() {

    suspend fun getCharacters() = getResult { requestApi.getAllCharacters() }
    suspend fun getCharacter(id: Int) = getResult {
        requestApi.getCharacter(id)
    }
}