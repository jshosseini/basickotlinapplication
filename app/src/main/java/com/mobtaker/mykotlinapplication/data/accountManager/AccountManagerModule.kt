package com.mobtaker.mykotlinapplication.data.accountManager

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AccountManagerModule {

    @Binds
    @Singleton
    abstract fun bindSampleService(
        accountManagerImpl: AccountManagerImpl
    ): AccountManager

//    @Binds
//    @Singleton
//    abstract fun bindHeaderInterceptor(
//        headerInterceptorImpl: HeaderInterceptorImpl
//    ): HeaderInterceptor

}