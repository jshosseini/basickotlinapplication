package com.mobtaker.mykotlinapplication.data.accountManager

import android.content.Context
import com.mobtaker.mykotlinapplication.model.ConfirmOTPResponse

interface AccountManager {
    fun saveAccount(accountData: ConfirmOTPResponse, context: Context)
    fun getAccount(context: Context): ConfirmOTPResponse
    fun removeAccount(context: Context)
    fun isLogin(context: Context): Boolean
}