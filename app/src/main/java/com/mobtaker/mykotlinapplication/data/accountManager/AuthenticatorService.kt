package com.mobtaker.mykotlinapplication.data.accountManager

import android.app.Service
import android.content.Intent
import android.os.IBinder
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

class AuthenticatorService : Service() {


    private lateinit var authenticator: Authenticator

    override fun onCreate() {
        authenticator = Authenticator(this)
    }
    override fun onBind(intent: Intent): IBinder? = authenticator.iBinder
}