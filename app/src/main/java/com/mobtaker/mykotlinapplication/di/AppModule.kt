package com.mobtaker.mykotlinapplication.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mobtaker.mykotlinapplication.data.AppDatabase
import com.mobtaker.mykotlinapplication.data.accountManager.AccountManager
import com.mobtaker.mykotlinapplication.data.accountManager.AccountManagerImpl
import com.mobtaker.mykotlinapplication.data.dao.CharacterDao
import com.mobtaker.mykotlinapplication.domain.RemoteRepository
import com.mobtaker.mykotlinapplication.domain.RequestApi

import com.mobtaker.mykotlinapplication.repository.Repository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl("https://rickandmortyapi.com/api/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideCharacterService(retrofit: Retrofit): RequestApi =
        retrofit.create(RequestApi::class.java)

    @Singleton
    @Provides
    fun provideCharacterRemoteDataSource(characterService: RequestApi) =
        RemoteRepository(characterService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideCharacterDao(db: AppDatabase) = db.characterDao()

    @Singleton
    @Provides
    fun provideRepository(
        remoteDataSource: RemoteRepository,
        localDataSource: CharacterDao
    ) =
        Repository(remoteDataSource, localDataSource)
}