package com.mobtaker.mykotlinapplication.view.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.mobtaker.mykotlinapplication.R
import com.mobtaker.mykotlinapplication.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        var binding:ActivityMainBinding= ActivityMainBinding.inflate(layoutInflater)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(binding.root)

       // val navHostFragment:NavHostFragment=supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        //val navController:NavController=navHostFragment.navController
       // val appBarConfiguration: AppBarConfiguration = AppBarConfiguration(navController.graph)
       // binding.toolbar.setupWithNavController(navController,appBarConfiguration)
    }
}