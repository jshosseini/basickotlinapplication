package com.mobtaker.mykotlinapplication.view.ui

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.rickandmorty.utils.autoCleared
import com.mobtaker.mykotlinapplication.R
import com.mobtaker.mykotlinapplication.databinding.FragmentSplashBinding
import com.mobtaker.mykotlinapplication.viewModel.SplashViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
@AndroidEntryPoint
class SplashFragment : Fragment() {
    private val viewModel: SplashViewModel by viewModels()
    private var binding by autoCleared<FragmentSplashBinding>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // in here you can do logic when backPress is clicked
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSplashBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    fun init() {

        lifecycleScope.launch {
            val animator = ValueAnimator.ofInt(1, 50)
            animator.duration = 3000
            animator.addUpdateListener { animation ->
                binding?.simpleProgressBar?.progress = animation.animatedValue as Int
                binding?.progressPercent?.text = (animation.animatedValue as Int).toString() + "%"
            }
            animator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    setupApp()
                }
            })
            binding.progressBarParent.apply {
                alpha = 0f
                visibility = View.VISIBLE

                animate()
                    .alpha(1f)
                    .setDuration(2000)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            animator?.start()
                        }
                    })
            }

        }
    }

    private fun setupApp() {
        viewModel.getUserStatus(requireContext()).observe(this) {
            if (it)
                checkToken()
            else {
                findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
            }
        }
    }

    private fun checkToken() {
        viewModel.getUser(requireContext()).observe(this) {
            findNavController().navigate(
                R.id.action_splashFragment_to_mainFragment
            )

        }
    }
}


