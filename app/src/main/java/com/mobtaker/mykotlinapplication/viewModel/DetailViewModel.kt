package com.mobtaker.mykotlinapplication.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.mobtaker.mykotlinapplication.model.Character
import com.mobtaker.mykotlinapplication.repository.Repository
import com.mobtaker.mykotlinapplication.utills.Resource

class DetailViewModel @ViewModelInject constructor(private var repository: Repository) :
    ViewModel()
{
    private val _id = MutableLiveData<Int>()
    private val _character = _id.switchMap { id -> repository.getCharacter(id) }
    val character: LiveData<Resource<Character>> = _character

    fun start(id: Int) {
        _id.value = id
    }
}