package com.mobtaker.mykotlinapplication.viewModel

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.mobtaker.mykotlinapplication.data.accountManager.AccountManager
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    val accountManager: AccountManager
) : ViewModel() {
    fun getUserStatus(context: Context) = liveData(Dispatchers.IO)
    {
        emit(accountManager.isLogin(context))
    }

    fun getUser(context: Context) = liveData(Dispatchers.IO)
    {
        emit(accountManager.getAccount(context))
    }
}